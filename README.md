Leaf gas exchange database
========================================

This database accompanies the publication "Lin et al. Optimal stomatal behaviour around the world. In review"

To compile a single CSV file with all data, follow the steps below. Otherwise, download individual raw data files from the `data` subfolder. The compiled CSV will be elsewhere once the manuscript has been accepted for publication.

* Clone this repository with your favorite git client, or download a ZIP following the link on the left ('Downloads, Download repository').
* The raw data files are included in the `data` subdirectory
* To compile them into a single CSV file (and RDS file for use in R), run the `make_database.R` script in R, or use the `Makefile`. The output files are then written to `output`.
* If you run the R script directly, make sure to install the `dplyr` package (or run `dependencies.R` one time).

**Contact**

*  Yan-Shih Lin (yanshihL@gmail.com)
*  Belinda Medlyn (belinda.medlyn@mq.edu.au)
*  Remko Duursma (remkoduursma@gmail.com)


Variable | Description | Units or allowed values
---------|-------------|------
Pathway | Photosynthetic pathway | C3, C4, CAM
Type  | Gymnosperm or angiosperm | 'gymnosperm', 'angiosperm'
Plantform   | Plant life form | 'tree', 'shrub', 'grass', 'crop', 'savanna' 
Leafspan  | Evergreen or Deciduous | 'evergreen', 'deciduous' 
Tregion  | Biome | 'arctic', 'boreal', 'temperate', 'tropical'
Wregion  | Classification based on aridity index | 'arid', 'semi-arid', 'dry sub-humid', 'humid' 
Wregion2  | Moisture index | (numerical)  
opt  | Growing under ambient or stressed condition | 'opt' or 'non-opt' 
fitgroup  | data grouping tag (optional) | (string)
Date  | Date of the year when measurement conducted |  
Time |Time of the day when measurement conducted |  
decimaltime  | 0 - 24 | Hours
Datacontrib  | Name of the data contributor | (string)
Species  | Species name | (string)
Funtype  |  Plant functional type (optional | (string) 
Funtype2v | Plant functional type, second definition| (string)
Location  | Site location | (string) 
VPD  | Vapour pressure deficit | kPa 
VPD_A  | Vapour pressure deficit gap filling based on air T | kPa
VPD_L  | Vapour pressure deficit gap filling based on leaf T| kPa
RH  | relative humidity | %
Tair  |  Air temperature | degrees C 
Tleaf  | Leaf temperature | degrees C 
CO2S  | CO2 concentration at leaf surface | ppm
PARin  | Photosynthetically active radiation in cuvette | umol m-2 s-1
Patm  | Atomospheric pressure | kPa
Photo  | Photosynthetic rate | umol m-2 s-1
Cond  | Stomatal conductance | mol m-2 s-1
BLCond  | Boundary layer conductance| mol m-2 s-1
Ci  | Intercelllular [CO2] | ppm 
Trmmol  | Leaf transpiration | mmol m-2 s-1 
Rdark  | Dark respiration rate | umol m-2 s-1 
PARout  | Photosynthetically active radiation in cuvette | umol m-2 s-1
SWC  | Soil water content | m3 m-3
SWP  | Soil water potential | MPa
latitude  | Site latitude | Decimal degrees
longitude  | Site longitude | Decimal degrees
altitude  | Site altitude | m
Totalheight  | Total plant height | m 
LAI  | Leaf area index | m2 m-2
sampleheight  | Height of measurement | m 
canopyposition  | | 'Sunlit' or 'shaded' leaf, 'top' or 'lower' canopy  
nperc  | Leaf nitrogen concentration | % (mass) 
SLA  | Specific leaf area | cm2 g-1
leafage  | Leaf age | years 
leafsize  | Leaf size | cm2 
leaflen  | Leaf length | cm 
LWP  | Leaf water potential | MPa 
LWPpredawn  | Pre-dawn leaf water potential | MPa 
Instrument  | Instrument used for measurement | e.g. Licor 6400, automated cuvette, etc
Season  | Season when the measurement conducted | (string)
GrowthCa  | Growth [CO2] | 'ambient' or ppm 
GrowthTair  | Growth Tair | 'ambient' or degree C 
Growthcond  | Growth condition | 'Glasshouse', ' Field', 'Potted plant', 'Whole-tree chamber', etc. 
Treatment  | Experimental treatment | 'fertilization', 'irrigation', etc. 
TreeNum  | Tree number | (string) 
LeafNum  | Leaf number | (string) 
OriginalFile  | Original file name from data contributor | (string)
Comments  | Anything useful related to the data | (string)
Reference  | Referee associated with the data set | (string)
LightSource  | Light source used in the cuvette| (string)